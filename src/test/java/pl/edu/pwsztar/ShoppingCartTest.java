package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShoppingCartTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup() {
        shoppingCart = new ShoppingCart();
    }

    @ParameterizedTest(name = "should add product with name {0} and price {1} and quantity {2} properly")
    @CsvSource({
            "Milk, 5, 10",
            "Cola, 15, 30",
            "Butter, 10, 50",
            "Apple, 20, 100",
            "Potato, 10, 150"
    })
    void shouldAddProductsProperly(String productName, int price, int quantity) {
        boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertTrue(result);
    }

    @ParameterizedTest(name = "should not add product with name {0} and price {1} and quantity {2}")
    @CsvSource({
            "Milk, -5, 10",
            "Butter, 5, 0",
            "Cola, 15, -30",
            ", 10, 50"
    })
    void shouldNotAddProductsWithInvalidData(String productName, int price, int quantity) {
        boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertFalse(result);
    }

    @Test
    void shouldDeleteProductsProperly() {
        shoppingCart.addProducts("Milk", 10, 5);
        shoppingCart.addProducts("Cola", 15, 20);
        shoppingCart.addProducts("Butter", 5, 30);

        boolean resultDeleteMilk = shoppingCart.deleteProducts("Milk", 5);
        boolean resultDeleteCola = shoppingCart.deleteProducts("Cola", 5);
        boolean resultDeleteColaAgain = shoppingCart.deleteProducts("Cola", 10);
        boolean resultDeleteButter = shoppingCart.deleteProducts("Butter", 1);

        assertTrue(resultDeleteMilk);
        assertTrue(resultDeleteCola);
        assertTrue(resultDeleteColaAgain);
        assertTrue(resultDeleteButter);
    }

    @Test
    void shouldNotDeleteProducts() {
        shoppingCart.addProducts("Milk", 10, 5);
        shoppingCart.addProducts("Cola", 15, 20);
        shoppingCart.addProducts("Butter", 5, 30);
        shoppingCart.addProducts("Bread", 5, 25);

        boolean resultDeleteMilk = shoppingCart.deleteProducts("Milk", 6);
        boolean resultDeleteCola = shoppingCart.deleteProducts("Cola", -5);
        boolean resultDeletePepsi = shoppingCart.deleteProducts("Pepsi", 10);
        boolean resultDeleteButter = shoppingCart.deleteProducts("Butter", 0);
        boolean resultDeleteBread = shoppingCart.deleteProducts("Bread", 26);

        assertFalse(resultDeleteMilk);
        assertFalse(resultDeleteCola);
        assertFalse(resultDeletePepsi);
        assertFalse(resultDeleteButter);
        assertFalse(resultDeleteBread);
    }

    @Test
    void shouldIncreaseProductQuantityWhileAddingTheSameProduct() {
        boolean resultAddMilkFirst = shoppingCart.addProducts("Milk", 100, 1);
        boolean resultAddMilkSecond = shoppingCart.addProducts("Milk", 100, 5);
        boolean resultAddMilkThird = shoppingCart.addProducts("Milk", 100, 15);

        assertTrue(resultAddMilkFirst);
        assertTrue(resultAddMilkSecond);
        assertTrue(resultAddMilkThird);
    }

    @Test
    void shouldNotIncreaseProductQuantityWhileChangingPrice() {
        boolean resultAddMilkFirst = shoppingCart.addProducts("Milk", 100, 1);
        boolean resultAddMilkSecond = shoppingCart.addProducts("Milk", 101, 5);
        boolean resultAddMilkThird = shoppingCart.addProducts("Milk", 100, 15);

        assertTrue(resultAddMilkFirst);
        assertFalse(resultAddMilkSecond);
        assertTrue(resultAddMilkThird);
    }

    @Test
    void shouldNotAddProductIfProductsLimitHasBeenReached() {
        boolean resultAddCola = shoppingCart.addProducts("Cola", 5, 1);
        boolean resultAddButterFirst = shoppingCart.addProducts("Butter", 100, 498);
        boolean resultAddButterSecond = shoppingCart.addProducts("Butter", 100, 1);
        boolean resultAddButterThird = shoppingCart.addProducts("Butter", 100, 1);

        assertTrue(resultAddCola);
        assertTrue(resultAddButterFirst);
        assertTrue(resultAddButterSecond);
        assertFalse(resultAddButterThird);
    }

    @Test
    void shouldGetQuantityOfProduct() {
        shoppingCart.addProducts("Butter", 1,2);
        shoppingCart.addProducts("Cola", 5, 10);
        shoppingCart.addProducts("Cola", 5, 10);

        int resultButter = shoppingCart.getQuantityOfProduct("Butter");
        int resultCola = shoppingCart.getQuantityOfProduct("Cola");
        int resultOfNotExistingProduct = shoppingCart.getQuantityOfProduct("Pepsi");

        assertEquals(2, resultButter);
        assertEquals(20, resultCola);
        assertEquals(0, resultOfNotExistingProduct);
    }

    @Test
    void shouldGetSumProductsPrices() {
        shoppingCart.addProducts("Milk", 10, 30);
        shoppingCart.addProducts("Butter", 15, 21);
        shoppingCart.addProducts("Cola", 20, 5);
        shoppingCart.deleteProducts("Cola", 2);
        int sumProductsPrices = shoppingCart.getSumProductsPrices();

        assertEquals(675, sumProductsPrices);
    }

    @Test
    void shouldGetSumProductsPricesEqualsToZeroIfShoppingCartIsEmpty() {
        int sumProductsPrices = shoppingCart.getSumProductsPrices();

        assertEquals(0, sumProductsPrices);
    }

    @Test
    void shouldGetProductPrice() {
        shoppingCart.addProducts("Cola", 20, 5);
        shoppingCart.deleteProducts("Cola", 2);
        int colaPrice = shoppingCart.getProductPrice("Cola");

        assertEquals(20, colaPrice);
    }

    @Test
    void shouldGetProductNames() {
        shoppingCart.addProducts("Cola", 20, 5);
        shoppingCart.addProducts("Butter", 15, 15);
        shoppingCart.addProducts("Milk", 10, 3);
        shoppingCart.addProducts("Cola", 20, 5);
        shoppingCart.addProducts("Pepsi", 20, 15);
        shoppingCart.deleteProducts("Pepsi", 15);
        List<String> result = shoppingCart.getProductsNames();
        List<String> names = Arrays.asList("Cola", "Butter", "Milk");

        assertTrue(names.containsAll(result));
    }

    @Test
    void shouldNotGetProductNamesIfShoppingCartIsEmpty() {
        List<String> result = shoppingCart.getProductsNames();

        assertTrue(result.isEmpty());
        assertEquals(0, result.size());
    }
}
