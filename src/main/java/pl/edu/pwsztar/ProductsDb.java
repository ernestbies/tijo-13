package pl.edu.pwsztar;

import java.util.Collection;
import java.util.HashMap;

public class ProductsDb {
    private HashMap<String, Product> products = new HashMap<>();

    public void addProduct(Product product) {
        products.put(product.getProductName(), product);
    }

    public void removeProductByName(String productName) {
        products.remove(productName);;
    }

    public Product getProductByName(String name) {
        return products.get(name);
    }

    public Collection<Product> getAllProducts() {
        return products.values();
    }

    public int getQuantityOfProducts() {
        return products.values().stream()
                .mapToInt(Product::getQuantity)
                .sum();
    }
}
