package pl.edu.pwsztar;

import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private final ProductsDb productsDb;

    public ShoppingCart() {
        productsDb = new ProductsDb();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if(price <= 0 || quantity <= 0 || productName == null || productName.isEmpty() || ((productsDb.getQuantityOfProducts() + quantity) > ShoppingCartOperation.PRODUCTS_LIMIT)) {
            return false;
        } else {
            Product foundedProduct = productsDb.getProductByName(productName);
            int quantityAdd = 0;
            if(foundedProduct != null) {
                if(foundedProduct.getPrice() != price){
                    return false;
                }
                quantityAdd = foundedProduct.getQuantity();
            }
            Product product = new Product(productName, price, quantity + quantityAdd);
            productsDb.addProduct(product);
            return true;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        Product foundedProduct = productsDb.getProductByName(productName);
        if(quantity <= 0 || foundedProduct == null || foundedProduct.getQuantity() < quantity) {
            return false;
        } else {
            if(foundedProduct.getQuantity() > quantity) {
                Product product = new Product(productName, foundedProduct.getPrice(), foundedProduct.getQuantity() - quantity);
                productsDb.addProduct(product);
            } else {
                productsDb.removeProductByName(productName);
            }
            return true;
        }
    }

    public int getQuantityOfProduct(String productName) {
        Product foundedProduct = productsDb.getProductByName(productName);
        if(foundedProduct == null) {
            return 0;
        } else {
            return foundedProduct.getQuantity();
        }
    }

    public int getSumProductsPrices() {
        return productsDb.getAllProducts()
                .stream()
                .mapToInt(product -> product.getPrice() * product.getQuantity())
                .sum();
    }

    public int getProductPrice(String productName) {
        Product foundedProduct = productsDb.getProductByName(productName);
        if(foundedProduct == null) {
            return 0;
        } else {
            return foundedProduct.getPrice();
        }
    }

    public List<String> getProductsNames() {
        return productsDb.getAllProducts()
                .stream()
                .map(Product::getProductName)
                .collect(Collectors.toList());
    }
}
